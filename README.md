# Assessment: FizzBuzz
This assessment will introduce you to a common programming puzzle called FizzBuzz. It requires that you use both loops and conditionals. Variations of this puzzle are widely used as an initial screening question during job interviews. Because this is a classic puzzle, it's easy to look up solutions online. It will be good practice for you to first try to solve the puzzle yourself before searching online.

Write a function named fizzBuzz that takes one parameter: maxValue.
You can write your JavaScript inline in your index.html, or link a seperate js file.
Your function should console.log() a string with comma separated values with no spaces.
This function should loop through 1 - maxValue (inclusive).
If a number is even, concatenate "Fizz, " to the end of your string.
If a number is a multiple of 3, concatenate "Buzz, " to the end of your string.
If a number is even and a multiple of 3 concatenate "FizzBuzz, " to the end of your string.
If a number is none of these things, concatenate the number itself and a ", " to the end of your string.
This function should console.log() this string after maxValue iterations of the loop.

# Bonus
Write a second function named fizzBuzzPrime.
This function will follow all of the rules of your fizzBuzz function and 1 additional rule:
If a number is a prime number, concatenate "Prime, " to the end of your string

# Submission
Push your code into your GitLab repository and use the GitLab Pages feature to allow your site to be viewed directly. In GitLab, please add KA_Grading as a member on your project with "Reporter" permission, and submit your gitlab pages url (Ex: https://username.gitlab.io/fizzbuzz).
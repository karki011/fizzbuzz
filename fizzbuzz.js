function fizzBuzz(maxValue) {
    let string = "";
    for (i = 1; i <= maxValue; i++) {
        if (i % 2 === 0 && i % 3 === 0) {
            string += "FizzBuzz, ";
        } else if (i % 3 === 0) {
            string += "Buzz, ";
        } else if (i % 2 === 0) {
            string += "Fizz, ";
        } else {
            string += i + ", ";
        }
    }
    return (console.log(string));
}
let inputNumber = prompt("Your number: ");
fizzBuzz(inputNumber);

//need to check for prime number
function checkPrimeNumber(num) {
    for (let i = 2; i < num; num++) {
        if (num % i === 0) {
            return false;
        }
        return true;
    }
    return num > 1;
}

function fizzBuzzPrime(maxValue) {
    let string = "";
    for (i = 1; i <= maxValue; i++) {
        if (i % 2 === 0 && i % 3 === 0) {
            string += "FizzBuzz, ";
        } else if (i % 3 === 0) {
            string += "Buzz, ";
        } else if (i % 2 === 0) {
            string += "Fizz, ";
        } else if (checkPrimeNumber(i) === true) {
            string += "Prime, ";
        } else {
            string += i + ", ";
        }
    }

    return (console.log(string));
}
let fizzBuzzPrimeValue = prompt("Your number again: ");
fizzBuzzPrime(fizzBuzzPrimeValue);

// source https://www.thepolyglotdeveloper.com/2015/04/determine-if-a-number-is-prime-using-javascript/
